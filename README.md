# LibreArp Linux build Docker

This repository provides a `Dockerfile` for building binaries of [LibreArp](https://librearp.gitlab.io/), a free-form pattern arpeggiator, for Linux.

The image is currently based on Ubuntu 20.04 (`focal`), meaning the resulting binaries should work on that version of Ubuntu and newer. It should also work on any distribution of Linux providing the same or newer versions of the libraries used by LibreArp.
